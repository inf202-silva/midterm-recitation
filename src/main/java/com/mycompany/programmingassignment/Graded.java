package com.mycompany.programmingassignment;

public class Graded {
    static void towerOfHanoi(int n, char from_rod, char to_rod, char helper_rod) {
        if (n == 1) {
            System.out.println("FIRST");
            System.out.println("Take disk 1 from rod " + from_rod +  " to rod " + to_rod);
            return;
        }
        System.out.println("SECOND");
        towerOfHanoi(n-1, from_rod, helper_rod, to_rod);
        System.out.println("THIRD");
        System.out.println("Take disk "+ n +" to rod "+ to_rod);
        towerOfHanoi(n-1, helper_rod, to_rod, from_rod);
        
    }
    public static void main(String[] args) {
        int n = 3;
        towerOfHanoi(n, 'A', 'C', 'B');
    }
}

