package com.mycompany.programmingassignment;

import java.io.File;  // Import the File Class so that you can use it
import java.io.IOException;  // Import the IOException class to look for errors within the code

public class FileHandling {
  public static void main(String[] args) throws IOException {
      try {
                  //Part1: Creating a file
      File FL = new File("C:\\Users\\Public\\Documents\\filename.txt");
      
      //Creates the File
      if (FL.createNewFile()) {
          //Get the file name if created
        System.out.println(FL.getName());
          //Print line if the file exists
      } else {
        System.out.println("File already exists.");
      } 
         
      } catch (IOException e) {
      System.out.println("An error occurred.");
    }
  }
}

