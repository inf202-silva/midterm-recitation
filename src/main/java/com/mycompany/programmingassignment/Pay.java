package com.mycompany.programmingassignment;


import java.util.Scanner;
public class Pay {
    public static void main(String[] args) {
        Scanner user = new Scanner(System.in);
        System.out.println("Input your Skill Level(1, 2, 3):");
        int skillLevel = user.nextInt();
        int HrlyRate = 0;
        switch (skillLevel) {
            case 1:
                 HrlyRate = 17;
                break;
            case 2:
                 HrlyRate = 20;
                break;
            case 3:
                 HrlyRate = 22;
                break;    
        }
        
            if (skillLevel > 0 && skillLevel < 4) {
                System.out.println("Enter Total of Hours Worked: ");
                int HrsWorked = user.nextInt();
                int OTHrs = 0;
                if (HrsWorked > 40) {
                    OTHrs = HrsWorked - 40;
                }
            double regularPay = HrsWorked * HrlyRate;
            double overtimePay = OTHrs * (HrlyRate * 1.5);
            int totalHrs = OTHrs + HrsWorked;
            
            double totalPay = regularPay + overtimePay;
            double medicalInsurance = 32.5;
            double dentalInsurance = 20.0;
            double longtermInsurance = 10.0;
            double rtrPlan = totalPay * 0.3;
            double itemizedDeduc = 0;
            int choice = 0;
            int choiceRpt = 0;
            
            if (skillLevel > 1 && skillLevel < 4) {
                do {
                    System.out.println("1) Medical Insurance.");
                    System.out.println("2) Dental Insurance.");
                    System.out.println("3) Long-term Disability Insurance.");
                    System.out.println("0) Skip.");
                    choice = user.nextInt();
                    choiceRpt ++;
                        if (choice == 1) {
                        itemizedDeduc += medicalInsurance;
                        System.out.println("Medical Insurance has been deducted from your pay.");  }
                    else if (choice == 2) {
                        itemizedDeduc += dentalInsurance;
                        System.out.println("Dental Insurance has been deducted from your pay.");  }
                    else if (choice == 3) {
                        itemizedDeduc += longtermInsurance;
                        System.out.println("Long-term Disability Insurance has been deducted from your pay."); }
                    else { 
                        System.out.println("You have not chosen an insurance."); 
                        choice = 0; } 
            } while (choice != 0 && choiceRpt < 3);
                if (skillLevel == 3) {
                    System.out.println("Would you like to invest in a Retirement Plan?");
                    System.out.println("1) Yes.");
                    System.out.println("2) No.");
                    int rtrC = user.nextInt();
                    if (rtrC == 1) {
                        System.out.println("Retirement Plan has been deducted from your pay.");
                        itemizedDeduc +=  rtrPlan; }
                }
            }
            double netPay = totalPay - itemizedDeduc;
                System.out.println();
                System.out.println("Hours worked: "+totalHrs);
                System.out.println("Hourly pay rate: $"+HrlyRate);
                System.out.println("Regular pay: $"+regularPay);
                System.out.println("Overtime pay: $"+overtimePay);
                System.out.println("Total pay: $"+totalPay);
                if (itemizedDeduc > totalPay) {
                    System.out.println("Error: Negative pay result"); }
                else {
                    System.out.println("Total itemized deductions: $"+itemizedDeduc);
                    System.out.println("Net pay: $"+netPay);
                }
        }
    }
}

