package com.mycompany.programmingassignment;

//Import every class needed for the code to run
import java.io.File;
import java.io.DataOutputStream;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;

public class FileHandlingLesson {
    public static void main(String[] args) throws Exception {
        //Calls for a directory and filename.
        File FL = new File("FileHandling.txt");
        
        //Creates the file
        FileOutputStream FOS = new FileOutputStream(FL);
        //Ready the file for writing
        DataOutputStream DOS = new DataOutputStream(FOS);
        //Writes in the files
        DOS.writeUTF("Hello Everyone! This is INF202.");
                
        //Calls the file for Reading
        FileInputStream FIS = new FileInputStream(FL);
        //Ready the file for Reading
        DataInputStream DIS = new DataInputStream(FIS);
        //Assigning variable for the text inside the file
        String text = DIS.readUTF();
        //Printing the text
        System.out.println(text);
    }
    
}
