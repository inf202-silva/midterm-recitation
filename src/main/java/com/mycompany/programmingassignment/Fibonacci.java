package com.mycompany.programmingassignment;


public class Fibonacci {
     public static void main(String[] args) {
   int maxnum = 10;

        System.out.println("Fibonacci series of " + maxnum + " numbers: ");

        for(int i = 0; i < maxnum; i++) {
            System.out.print(fib(i) + " ");
        }

    }
   public static int fib(int n) {
            if(n == 0) {
                return 0;
            }
            if(n == 1 || n == 2) {
                return 1;
            }
            return fib(n - 2) + fib(n -1);
        }
  } 

